import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import router from './routes'
import VueSweetalert2 from 'vue-sweetalert2';
import Loading from 'vue-loading-overlay';
import { ClientTable } from 'vue-tables-2';

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/scss/style.scss'
import VueRouter from 'vue-router'
import 'sweetalert2/dist/sweetalert2.min.css';
import 'vue-loading-overlay/dist/vue-loading.css';

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(Loading, {
  backgroundColor: '#ffffff50',
  loader: 'spinner',
  color: '#4aa3bd'
});
Vue.use(ClientTable, {
  texts: {
    count: "Mostrando del {from} al {to} de {count} registro | {count} registros | 1 Registro",
    first: "First",
    filter: "",
    records: "......",
    filterPlaceholder: "Buscar registro",
    limit: "",
    page: "page",
    noResults: "Lo sentimos, pero no encontramos tu busqueda",
    filterBy: "{column}",
    loading: "Cargando",
    defaultOptions: "Todas",
    columns: "Columns"
  }
})



Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
