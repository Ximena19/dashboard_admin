import VueRouter from "vue-router";

/**
 * Rutas del proyecto
 */
const routes = [
    {
        path: '/', 
        component: () => import('../Views/Home/home.vue'), 
        name: 'Home',
        BeforeEnter: (to, from, next) => {
            let user = JSON.parse(localStorage.getItem('user'));
            console.log(to, from, next);
            if(!user){
                next('/Login');
            }else{
                next();
            }
        }
    },
    {
        path: '/login',
        component: () => import('../Views/auth/Login/login.vue'), 
        name: 'Login'
    },
    {
        path: '/admin',
        component: () => import('../Views/admin/admin.vue'),
        name: 'Admin'
    },
];

const router = new VueRouter({
    mode: 'history',
    routes 
});

/**
 * Condicion de personas logueadas para acceso al admin.
 */
router.beforeEach((to, from, next) => {
    let user = JSON.parse(localStorage.getItem('user'));
    if(to.name !== 'Login' && !user) {
        next('/login');
    }else if(to.name === 'Login' && user){
        next('/');
    }
    else {
        next();
    }
})
export default router;